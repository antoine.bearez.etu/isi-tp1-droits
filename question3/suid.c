#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
        FILE *f;
        uid_t ruid;
        uid_t euid;
        gid_t rgid;
        gid_t egid;
        char s;

        ruid = getuid();
        euid = geteuid();
        rgid = getgid();
        egid = getegid();

        printf("ruid = %i, euid = %i, rgid = %i, egid = %i", ruid, euid, rgid, egid);


        if (argc < 2) {
                printf("Missing argument\n");
                exit(EXIT_FAILURE);
        }
        printf("Hello world\n");
        f = fopen(argv[1], "r");