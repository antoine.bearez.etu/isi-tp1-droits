# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: BEAREZ Antoine, antoine.bearez.etu@univ-lille.fr , M1 RVA

- Nom, Prénom, email: ___

Les questions 7 à 10 n'ont pas été faites.

## Question 1

Si Toto ne faisait pas parti du groupe ubuntu, le processus ne pourrait pas écrire car il ne possède pas le droit d'écriture propriétaire.
Cependant, comme ubuntu a le droit d'écriture sur le fichier, l'utilisateur toto peut aussi écrire dans le fichier titi.txt


## Question 2

Le couple rx donne le droit d'examiner le répertoire et son contenu.

L'utilisateur toto ne peut pas acceder au repertoire mydir car toto étant dans le groupe ubuntu qui n'a pas le droit d'"executer"
le repertoire, toto ne peut lui non plus pas entrer dans le repertoire.

Des points d'interrogation (?) sont affichés à la place de toutes les données (propriétaire, groupe, accessibilité etc), seul les noms des fichiers/répertoires apparaissent.
Cela est du au fait que toto n'a pas le droit d'accès au repertoire mydir, donc il ne peut pas
obtenir des informations sur les fichiers/repertoires contenus dans mydir.

## Question 3

ruid = 1001, euid = 1001, rgid = 1000, egid = 1000
Non, le processus n'arrive pas à ouvrir le fichier mydir/data.txt en lecture:
Cannot open file: Permission denied

Après le flag set-user-id:
ruid = 1001, euid = 1000, rgid = 1000, egid = 1000
Oui, le processus arrive à ouvrir le fichier mydir/data.txt
File opens correctly

## Question 4

euid :  1001  ,egid :  1000

## Question 5

La commande chfn permet de modifier les informations "finger" d'un 
utilisateur. elles sont stockées dans le fichier /etc/passwd et
contiennent les informations importantes voire sensibles sur 
l'utilisateur.

ubuntu@vmisi:/etc$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

Le fichier est lisible et il peut être écrit par le propriétaire ubuntu,
Les membres du groupe root ne peuvent que lire et executer, et les autres utilisateurs ne peuvent également que lire et executer.
On remarque le s qui indique la permission d'exécution avec set-user-id.

^
Avant chfn:
toto:x:1001:1000:,,,:/home/toto:/bin/bash

Après chfn:
toto:x:1001:1000:,13,0000,1111:/home/toto:/bin/bash
La commande chfn a bien modifié le contenu de passwd

## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier /etc/shadow.
Ces mots de passe sont cryptés (dans notre cas, on peut lire pour l'utilisateur toto $6
ce qui signifie que le mot de passe a été hashé avec la méthode d'encryption SHA512.

On les stocke dans ce fichier car seul le root et le group shadow peuvent lire son contenu et seul root peut écrire.
De cette façon, les mots de passe ne sont pas "volables" par des personnes qui s'introduisent dans le système.


## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








